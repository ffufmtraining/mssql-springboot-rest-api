package com.example.demo.controllers

import com.example.demo.entities.Categories
import com.example.demo.entities.CategoriesRepository
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("categories")
class CategoriesController(val categoriesRepository: CategoriesRepository) {

  @PostMapping
  fun newCategory(@RequestBody categories: Categories): Categories {
    return categoriesRepository.save(categories)
  }
}