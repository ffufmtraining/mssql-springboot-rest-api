package com.example.demo.entities

import javax.persistence.*

@Table(name = "categories")
@Entity
open class Categories {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  open var id: Long? = null

  @Column(name = "code", nullable = false)
  open var code: String? = null

  @Column(name = "name")
  open var name: String? = null
}