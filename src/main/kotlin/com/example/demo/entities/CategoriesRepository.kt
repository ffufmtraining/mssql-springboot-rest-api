package com.example.demo.entities;

import org.springframework.data.jpa.repository.JpaRepository

interface CategoriesRepository : JpaRepository<Categories, Long> {
}